# JupiterUI

> An elegant and reliable UI kit for web artisans.

**TL;DR** Visit the [Wiki](https://codeberg.org/xplato/JupiterUI/wiki) to learn how to use Jupiter.

## About Version 5

Version 5.0.0 was a complete purge. I refocused the purpose of Jupiter to fit more nicely in an increasingly JS-framework-oriented web. The JavaScript of past versions is gone forever. About half of the components are gone. Dropdowns, accordions, and so on: gone.

Why? Because that was too opinionated.

I mean, what are the chances that your new web app will precisely match and require the default component styles that Jupiter offered? Maybe only a few.

In the last year or so I've been using Jupiter, I've never touched most of the component classes—simply because they were too opinionated for my project. As such, I had to remove them. Bloating the CSS dist file with unnecessary code is something I'm not fond of.

To you, this probably doesn't make much difference. Jupiter still has the core system—layout, typography, colors, motion, and so on. It just doesn't contain what isn't useful or relevant anymore.

## About

JupiterUI is a front-end UI kit for modern websites. It provides a ton of styles for nearly any layout or component your website will use. It has got great defaults so that modification isn't always necessary for many projects. Moreover, it's easy to learn and easy to memorize.

## Features

- Built-in, _working_ components by default!
- _Zero_ dependencies!
- Excellent customization support out-of-the-box
- **Advanced colors** and _themeing_ system (Dark & Light mode out-of-the-box)
- Detailed layout system—with lots of support for Flex and Grid layouts.
- Easily extendable

## How will this help me?

If you're looking for a design system that gets out of the way to let you focus on your project, you're in the right place. There's no overhead to Jupiter. It's a plug-and-play library. Once you learn the rules, it'll cut your style effort down a lot.

### Is it production ready?

Absolutely. I'm using Jupiter in over 30 high-priority projects for some of my clients. I've ditched Tailwind and Bootstrap (and custom CSS!) in favor of it.

## Usage

Simply download the `dist/jupiterui.css` file and add it to your project.

Visit the [Wiki](https://codeberg.org/xplato/JupiterUI/wiki) to learn how to use Jupiter.

## Made with Jupiter

- [Gofë](https://gofe.app)
- [xplato.dev (my website)](https://hydra.im)
- [Infinium](https://infinium.earth)
- [Kiri](https://kiri.vercel.app)
- and a lot more... (Send a PR if you use Jupiter!)

## Licenses

A very small amount of third-party inspiration and/or code was used in this project.

There are three primary projects to thank:

- Thanks to the Carbon Design System ([License](https://github.com/carbon-design-system/carbon/blob/main/LICENSE)). A very small amount of information was used from this project. Primarily:
    - [Motion values](https://carbondesignsystem.com/guidelines/motion/overview/);
- Nicolas Gallagher and Jonathan Neal for `normalize.css` ([License](https://github.com/necolas/normalize.css/blob/master/LICENSE.md), [GitHub](https://github.com/necolas/normalize.css))
- Thanks to TailwindCSS for their extensive color system.